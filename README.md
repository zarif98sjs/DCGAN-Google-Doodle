# **`DCGAN Google Doodle`**

Unsupervised learning to create doodle images using `DCGAN` (Deep Convolutional GAN)

`DCGAN Paper` : https://arxiv.org/pdf/1511.06434.pdf

`Dataset` : [Google Quickdraw Dataset](https://console.cloud.google.com/storage/browser/quickdraw_dataset/full/numpy_bitmap;tab=objects?prefix=&forceOnObjectsSortingFiltering=false)


## Birthday Cake Doodle

![](birthday_cake.gif)